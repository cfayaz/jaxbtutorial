package com.fayaz.jaxbtutorial.test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.fayaz.jaxbtutorial.pojo.Book;
import com.fayaz.jaxbtutorial.pojo.BookStore;

public class ReadTest {
	
	private static final String BOOKSTORE_XML = "./bookstore-jaxb.xml";

	public static void main(String[] args) throws JAXBException, FileNotFoundException {
		JAXBContext context = JAXBContext.newInstance(BookStore.class);
        Unmarshaller unMarshaler = context.createUnmarshaller();
        BookStore bookstore2 = (BookStore) unMarshaler.unmarshal(new FileReader(
                BOOKSTORE_XML));
        ArrayList<Book> list = bookstore2.getBooksList();
        for (Book book : list) {
            System.out.println("Book: " + book.getName() + " from "
                    + book.getAuthor());
        }

	}

}
